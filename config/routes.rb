Rails.application.routes.draw do
 
  # get '/instalacoes_sensores/:installation_id', to: "sensor_installation#index", as: :sensor_installation
  # get '/instalacoes_sensores/:installation_id/new', to: "sensor_installation#new"
  

  resources :installations, path: "instalacoes" do
    patch '/sensores/:id',to: "sensor_installation#update" ,as: "installation_sensors"
    resources :sensor_installation, path: "sensores", as: :sensors
  end
  resources :equipment, path: "equipamentos" do
    patch '/sensores/:id',to: "sensor_equipment#update" ,as: "equipment_sensors"
    resources :sensor_equipment, path: "sensores", as: :sensors
  end
  resources :installations, path: :instalacoes
  devise_for :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'installations#index'
end
