class AddMonitorableToSensor < ActiveRecord::Migration[5.2]
  def change
    add_reference :sensors, :monitorable, polymorphic: true
  end
end
