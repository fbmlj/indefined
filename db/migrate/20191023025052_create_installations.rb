class CreateInstallations < ActiveRecord::Migration[5.2]
  def change
    create_table :installations do |t|
      t.string :name
      t.integer :user_id
      t.string :campus
      t.string :complement

      t.timestamps
    end
  end
end
