# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

5.times do |i|
  User.create! name: "User #{i + 1}", email: "user#{i + 1}@email.com",
               password: '123123'
end

10.times do |i|
  Installation.create! name: Faker::Nation.capital_city,
                       user: User.order('Random()').first,
                       campus: Faker::University.name,
                       complement: "Sala #{i + 300}"
end

20.times do
  Equipment.create! name: Faker::Device.model_name,
                    installation: Installation.order('Random()').first
Sensor.create(identifier: 1, monitorable: Installation.first)
Sensor.create(identifier: 2, monitorable: Installation.first)


Sensor.create(identifier: 3, monitorable: Equipment.first)
Sensor.create(identifier: 4, monitorable: Equipment.first)
end
