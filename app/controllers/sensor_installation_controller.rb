# frozen_string_literal: true
454
class SensorInstallationController < ApplicationController
  before_action :set_installation, only: %i[show edit create update destroy index]
  before_action :set_sensor, only: %i[show edit update destroy]
  
  def show
  end

  def new
    @sensor = Sensor.new
  end
  
  def index; end

  def edit; end

  def update
    respond_to do |format|
      @sensor.monitorable = @installation
      if @sensor.update(sensor_params)
        format.html { redirect_to @sensor, notice: 'Sensor foi atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @sensor }
      else
        format.html { render :edit }
        format.json { render json: @sensor.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @sensor = Sensor.new(sensor_params)

    @sensor.monitorable = @installation
    
    respond_to do |format|
      if @sensor.save
        format.html { redirect_to equipment_sensors_path(@sensor.monitorable), notice: 'Sensor foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @sensor }
      else
        format.html { render :new }
        format.json { render json: @sensor.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @sensor.destroy
    respond_to do |format|
      format.html { redirect_to installation_sensors_path, notice: 'Sensor foi destruido com sucesso' }
      format.json { head :no_content }
    end
  end

  private

  def set_installation
    @installation = Installation.find(params[:installation_id])
  end

  def set_sensor
    @sensor = Sensor.find(params[:id])
  end
  def sensor_params
    params.require(:sensor).permit(:identifier)
  end
end
