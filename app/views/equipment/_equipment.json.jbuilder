json.extract! equipment, :id, :name, :installation_id, :created_at, :updated_at
json.url equipment_url(equipment, format: :json)
