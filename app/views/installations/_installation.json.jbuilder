json.extract! installation, :id, :name, :user_id, :campus, :complement, :created_at, :updated_at
json.url installation_url(installation, format: :json)
