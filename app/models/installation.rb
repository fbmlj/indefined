class Installation < ApplicationRecord
  validates :name, :campus, :complement, presence: true
  has_many :sensors, as: :monitorable
  belongs_to :user
  has_many :equipments

  def length_recursive
    sum = sensors.length
    equipments.each do |e|
      sum += e.sensors.length
    end
    sum
  end
end
