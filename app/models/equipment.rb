class Equipment < ApplicationRecord
  belongs_to :installation
  validates :name, presence: true
  has_many :sensors, as: :monitorable
end
