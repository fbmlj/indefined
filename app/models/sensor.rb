class Sensor < ApplicationRecord
  validates :identifier, presence: true, uniqueness: true
  belongs_to :monitorable, polymorphic: true
end
